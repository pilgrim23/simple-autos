package com.keagan.autos;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/api/autos")
public class AutosController {

    AutoService autoService;

    public AutosController(AutoService autoService) {
        this.autoService = autoService;
    }

    @ExceptionHandler
    @ResponseStatus(HttpStatus.BAD_REQUEST)
    public void InvalidAutoExceptionHandler(InvalidAutoException exception) {
    }

    @GetMapping
    public ResponseEntity<AutoList> getAllAutos(@RequestParam(required = false) String make,
                                                @RequestParam(required = false) String color) {

        AutoList autoList;
        if (color == null && make == null) {
            autoList = autoService.getAllAutos();
        } else {
            autoList = autoService.getAllAutos(make, color);
        }
        return autoList == null ? ResponseEntity.noContent().build() :
                ResponseEntity.ok(autoList);
    }

    @PostMapping
    public Auto addAuto(@RequestBody Auto auto) {
        return autoService.addAuto(auto);
    }

    @GetMapping("/{vin}")
    public ResponseEntity<Auto> getAutoByVin(@PathVariable String vin) {
        Auto foundAuto = autoService.getAutoByVin(vin);
        if (foundAuto == null) {
            return ResponseEntity.noContent().build();
        } else {
            return ResponseEntity.ok(foundAuto);
        }
    }

    @PatchMapping("/{vin}")
    public ResponseEntity<Auto> updateAutoByVin(@PathVariable String vin,
                                                @RequestBody UpdateOwnerRequest body) {
        Auto updatedAuto = autoService.updateAutoByVin(vin, body.getOwner(), body.getColor());
        if (updatedAuto == null) {
            return ResponseEntity.noContent().build();
        }
        return ResponseEntity.ok(updatedAuto);
    }

    @DeleteMapping("/{vin}")
    public ResponseEntity<?> deleteAutoByVin(@PathVariable String vin) {
        try {
            autoService.deleteAuto(vin);
        } catch (AutoNotFoundException e) {
            return ResponseEntity.noContent().build();
        }
        return ResponseEntity.accepted().build();
    }
}
