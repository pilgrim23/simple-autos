package com.keagan.autos;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.util.List;

public interface AutoRepository extends JpaRepository<Auto, Long> {
    @Query(nativeQuery = true, value = "select * from autos where make like ? and color like ?")
    List<Auto> findAllByMakeAndColor(String make, String color);
    Auto findByVin(String vin);
}
