package com.keagan.autos;

import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class AutoService {

    AutoRepository autoRepository;

    public AutoService(AutoRepository autoRepository) {
        this.autoRepository = autoRepository;
    }

    public AutoList getAllAutos() {
        List<Auto> foundAutos = autoRepository.findAll();
        if (foundAutos.isEmpty()) {
            return null;
        }
        return new AutoList(foundAutos);
    }

    public AutoList getAllAutos(String make, String color) {
        if (make == null) make = "";
        if (color == null) color = "";
        List<Auto> foundAutos = autoRepository.findAllByMakeAndColor(make + "%", color + "%");
        if (foundAutos.isEmpty()) {
            return null;
        }
        return new AutoList(foundAutos);
    }

    public Auto addAuto(Auto auto) {
        return autoRepository.save(auto);
    }

    public Auto getAutoByVin(String vin) {
        return autoRepository.findByVin(vin);
    }

    public Auto updateAutoByVin(String vin, String owner, String color) {
        Auto foundAuto = autoRepository.findByVin(vin);
        if (foundAuto == null) return null;
        foundAuto.setOwner(owner);
        foundAuto.setColor(color);
        return autoRepository.save(foundAuto);
    }

    public void deleteAuto(String vin) {
        Auto foundAuto = autoRepository.findByVin(vin);
        if (foundAuto == null) throw new AutoNotFoundException();
        autoRepository.delete(foundAuto);
    }
}
