package com.keagan.autos;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;

import static org.hamcrest.collection.IsCollectionWithSize.hasSize;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import java.util.ArrayList;
import java.util.List;

@WebMvcTest(AutosController.class)
public class AutosControllerTest {

    @Autowired
    MockMvc mockMvc;

    @MockBean
    AutoService autoService;

    List<Auto> autos;

    public String toJSON(Object o) throws JsonProcessingException {
        ObjectMapper mapper = new ObjectMapper();
        return mapper.writeValueAsString(o);
    }

    @BeforeEach
    void setUp() {
        autos = new ArrayList<>();
        for (int i = 0; i < 10; i++) {
            Auto auto = new Auto("vin" + i, "ford", "t", 1931, "black");
            autos.add(auto);
        }
    }

    // GET /api/autos returns all autos 200
    @Test
    void getAllAutos_NoParams_returnsList() throws Exception {

        when(autoService.getAllAutos()).thenReturn(new AutoList(autos));

        mockMvc.perform(get("/api/autos"))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.autos", hasSize(10)));
    }

    // GET /api/autos returns 204 for no results
    @Test
    void getAutos_NoParams_returnsNoContent() throws Exception {
        when(autoService.getAllAutos()).thenReturn(null);

        mockMvc.perform(get("/api/autos"))
                .andExpect(status().isNoContent());
    }

    // GET /api/autos?make=ford returns all autos with make of ford
    // GET /api/autos?color=blue returns all autos with color blue
    // GET /api/autos?color=blue&make="ford" returns all fords with color blue
    @Test
    void getAutos_Make_returnsListWithMakeAndColor() throws Exception {

        when(autoService.getAllAutos(anyString(), anyString())).thenReturn(new AutoList(autos));

        mockMvc.perform(get("/api/autos?make=ford&color=red"))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.autos", hasSize(10)));
    }

    // POST /api/autos returns created auto 200
    @Test
    void addAuto_newAuto_returnsNewAuto() throws Exception {
        Auto auto = autos.get(0);

        when(autoService.addAuto(any(Auto.class))).thenReturn(auto);

        mockMvc.perform(post("/api/autos")
                .contentType(MediaType.APPLICATION_JSON)
                .content(toJSON(auto)))
                .andExpect(status().isOk())
                .andExpect(jsonPath("vin").value(auto.getVin()));
    }

    // POST /api/autos return bad request 400
    @Test
    void addAuto_badAuto_BadRequest() throws Exception {
        Auto auto = autos.get(0);

        when(autoService.addAuto(any(Auto.class))).thenThrow(InvalidAutoException.class);
        mockMvc.perform(post("/api/autos")
                .contentType(MediaType.APPLICATION_JSON)
                .content(toJSON(auto)))
                .andExpect(status().isBadRequest());
    }

    // GET api/autos/{vin} returns auto with matching vin 200
    @Test
    void getAuto_Vin_returnsAuto() throws Exception {
        Auto auto = autos.get(0);

        when(autoService.getAutoByVin(anyString())).thenReturn(auto);

        mockMvc.perform(get("/api/autos/" + auto.getVin()))
                .andExpect(status().isOk())
                .andExpect(jsonPath("vin").value(auto.getVin()));
    }

    // GET api/autos/{vin} returns no autos if not matching 204
    @Test
    void getAuto_Vin_NoContent() throws Exception {
        Auto auto = autos.get(0);
        when(autoService.getAutoByVin(anyString())).thenReturn(null);
        mockMvc.perform(get("/api/autos/" + auto.getVin()))
                .andExpect(status().isNoContent());
    }

    // PATCH api/autos/{vin} updates auto returns updated auto 200
    @Test
    void updateAuto_VinOwnerColor_returnsUpdatedAuto() throws Exception {
        Auto auto = autos.get(0);
        auto.setOwner("joe");
        auto.setColor("blue");
        when(autoService.updateAutoByVin(anyString(), anyString(), anyString())).thenReturn(auto);

        String requestBody = toJSON(new UpdateOwnerRequest("joe", "blue"));
        mockMvc.perform(patch("/api/autos/" + auto.getVin())
                .contentType(MediaType.APPLICATION_JSON)
                .content(requestBody))
                .andExpect(status().isOk())
                .andExpect(jsonPath("vin").value(auto.getVin()))
                .andExpect(jsonPath("owner").value(auto.getOwner()))
                .andExpect(jsonPath("color").value(auto.getColor()));
    }

    // PATCH api/autos/{vin} returns no autos if not matching 204
    @Test
    void updateAuto_VinOwnerColor_returnsNoContent() throws Exception {
        String requestBody = toJSON(new UpdateOwnerRequest("joe", "blue"));

        when(autoService.updateAutoByVin(anyString(), anyString(), anyString())).thenReturn(null);
        mockMvc.perform(patch("/api/autos/vinNumber")
                .contentType(MediaType.APPLICATION_JSON)
                .content(requestBody))
                .andExpect(status().isNoContent());
    }

    // PATCH api/autos/{vin} returns bad request 400
    @Test
    void updateAuto_badAuto_BadRequest() throws Exception {
        Auto auto = autos.get(0);
        String requestBody = toJSON(new UpdateOwnerRequest("joe", "blue"));

        when(autoService.updateAutoByVin(anyString(), anyString(), anyString()))
                .thenThrow(InvalidAutoException.class);

        mockMvc.perform(patch("/api/autos/" + auto.getVin())
                .contentType(MediaType.APPLICATION_JSON)
                .content(toJSON(requestBody)))
                .andExpect(status().isBadRequest());
    }

    // DELETE api/autos/{vin} updates auto returns accepted (202)
    @Test
    void deleteAuto_vin_returnsAccepted() throws Exception {
        Auto auto = autos.get(0);

        mockMvc.perform(delete("/api/autos/" + auto.getVin()))
                .andExpect(status().isAccepted());

        verify(autoService).deleteAuto(anyString());
    }

    // DELETE api/autos/{vin} returns no autos if not matching 204
    @Test
    void deleteAuto_vin_AutoNotFoundException() throws Exception {
        Auto auto = autos.get(0);
        doThrow(new AutoNotFoundException()).when(autoService).deleteAuto(anyString());
        mockMvc.perform(delete("/api/autos/missingVin"))
                .andExpect(status().isNoContent());
    }
}
